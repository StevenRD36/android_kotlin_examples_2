package com.example.exampleapp

import android.net.Uri
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.MediaController
import kotlinx.android.synthetic.main.activity_media.*

class MediaActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_media)
        var videoPath = "android.resource://"+packageName+"/"+R.raw.buffalo
        println(videoPath)
        var videoUri = Uri.parse(videoPath)
        videoView.setVideoURI(videoUri)

        var controller = MediaController(this)
        videoView.setMediaController(controller)
        controller.setAnchorView(videoView)
    }
}
