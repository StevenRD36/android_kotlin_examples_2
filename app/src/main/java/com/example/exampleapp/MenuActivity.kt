package com.example.exampleapp

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast

class MenuActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_menu)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu,menu)
        return true;
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when(item?.itemId){
            R.id.item1 -> Toast.makeText(this,"$item should be item 1 was pressed",Toast.LENGTH_LONG).show()
            R.id.item2 -> Toast.makeText(this,"$item should be item 2 was pressed",Toast.LENGTH_LONG).show()
            R.id.item3 -> Toast.makeText(this,"$item should be item 3 was pressed",Toast.LENGTH_LONG).show()
            R.id.item4 -> Toast.makeText(this,"$item should be item 4 was pressed",Toast.LENGTH_LONG).show()
            else -> Toast.makeText(this,"$item should be false was pressed",Toast.LENGTH_LONG).show()
        }
        return true;
    }
}
